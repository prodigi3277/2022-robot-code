// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import frc.robot.commands.AutonomousCommand;
import frc.robot.commands.BrakeUnenableCommand;
import frc.robot.commands.ClimberDownCommand;
import frc.robot.commands.ClimberUpCommand;
import frc.robot.commands.ClimberUpOverride;
import frc.robot.commands.DebugCommand;
import frc.robot.commands.DrivetrainCommand;
import frc.robot.commands.EngageTheBrakeCommand;
import frc.robot.commands.FirstIntakeEngageCommand;
import frc.robot.commands.LimelightTargeting;
import frc.robot.commands.LowerIntakeManCommand;
import frc.robot.commands.LowerIntakeReverseManCommand;
import frc.robot.commands.ShootViaLimelightCommand;
import frc.robot.subsystems.TurretAndShooter;
import frc.robot.commands.TurretTurnLeftCommand;
import frc.robot.commands.TurretTurnRightCommand;
import frc.robot.commands.UpperIntakeManCommand;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Feeder;
import frc.robot.subsystems.nyoom;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  private XboxController m_controller;
  private XboxController m_controllerB;

  // The robot's subsystems and commands are defined here...
  private nyoom m_nyoom;
  private Climber m_Climber;
  private AutonomousCommand m_autonomous; 
  private Feeder m_feeder;
  //private final OI m_oi = new OI(m_controller);
  private TurretAndShooter m_TurretAndShooter;
  

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    m_controller = new XboxController(Constants.CONTROLLER_PORT);
    m_controllerB = new XboxController(1);
    m_nyoom = new nyoom();
    m_feeder = new Feeder();
    m_TurretAndShooter = new TurretAndShooter();
    m_autonomous = new AutonomousCommand(m_TurretAndShooter, m_nyoom, m_feeder);
    m_Climber = new Climber();


    m_nyoom.setDefaultCommand(new DrivetrainCommand(m_nyoom, () -> m_controllerB.getLeftY(), () -> m_controllerB.getLeftX(), () -> m_controllerB.getRightX()));
    m_feeder.setDefaultCommand(new DebugCommand(m_feeder));
 //   m_TurretAndShooter.setDefaultCommand(new ShootingCommand(m_TurretAndShooter));

    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    final JoystickButton LowerIntakeManButton = 
        new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_A_BUTTON.getValue());
    final JoystickButton LowerIntakeReverseManButton = 
        new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_B_BUTTON.getValue());
    final JoystickButton UpperIntakeManButton = 
        new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_X_BUTTON.getValue());
    final JoystickButton ClimberUpButton = 
         new JoystickButton(m_controllerB, Constants.XBOX_BUTTON.XBOX_Y_BUTTON.getValue());
    final JoystickButton ClimberDownButton = 
         new JoystickButton(m_controllerB, Constants.XBOX_BUTTON.XBOX_B_BUTTON.getValue());
    final JoystickButton ClimberBreakButton = 
         new JoystickButton(m_controllerB, Constants.XBOX_BUTTON.XBOX_BACK_BUTTON.getValue());
         final JoystickButton ClimberNoBreakButton = 
         new JoystickButton(m_controllerB, Constants.XBOX_BUTTON.XBOX_START_BUTTON.getValue());
    final JoystickButton FirstFeederButton = 
         new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_Y_BUTTON.getValue());
         final JoystickButton turretTurnLeftButton = 
         new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_LEFT_SHOLDER_BUTTON.getValue());
         final JoystickButton turretTurnRightButton = 
         new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_RIGHT_SHOLDER_BUTTON.getValue());
   // final JoystickButton LowerPortShooterButton = new JoystickButton(m_controller,  Constants.XBOX_BUTTON.XBOX_RIGHT_SHOLDER_BUTTON.getValue());
    final JoystickButton ClimberUpOverrideButton
     = new JoystickButton(m_controllerB,  Constants.XBOX_BUTTON.XBOX_X_BUTTON.getValue());
     final JoystickButton targetUsingLimelightButton =
      new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_START_BUTTON.getValue());
      final JoystickButton shootUsingLimelightButton =
      new JoystickButton(m_controller, Constants.XBOX_BUTTON.XBOX_BACK_BUTTON.getValue());



        LowerIntakeManButton.whenHeld(new LowerIntakeManCommand(m_feeder));
        LowerIntakeReverseManButton.whenHeld(new LowerIntakeReverseManCommand(m_feeder));
        ClimberUpButton.whenPressed(new ClimberUpCommand(m_Climber));
        ClimberDownButton.whenHeld(new ClimberDownCommand(m_Climber));
        ClimberUpOverrideButton.whenHeld(new ClimberUpOverride(m_Climber));

        UpperIntakeManButton.whenHeld(new UpperIntakeManCommand(m_feeder));
        ClimberBreakButton.whenHeld(new EngageTheBrakeCommand(m_Climber));
        ClimberNoBreakButton.whenHeld(new BrakeUnenableCommand(m_Climber));
        turretTurnLeftButton.whenHeld(new TurretTurnLeftCommand(m_TurretAndShooter));
        turretTurnRightButton.whenHeld(new TurretTurnRightCommand(m_TurretAndShooter));
     // LowerPortShooterButton.whenHeld(new LowerPortShooterCommand(m_TurretAndShooter));
        FirstFeederButton.whenHeld(new FirstIntakeEngageCommand(m_feeder));
    targetUsingLimelightButton.whileHeld(new LimelightTargeting(m_TurretAndShooter));
    shootUsingLimelightButton.whileHeld(new ShootViaLimelightCommand(m_TurretAndShooter));





  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return m_autonomous;
  }
}
