// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.TurretAndShooter;

public class TurretTurnLeftCommand extends CommandBase {
  TurretAndShooter m_turet;
  /** Creates a new TurretTurnLeftCommand. */
  public TurretTurnLeftCommand(TurretAndShooter turretAndShooter) {
    m_turet = turretAndShooter;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_turet);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    m_turet.turrenTurnLeft();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_turet.turretTurnStop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
