// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
//import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.subsystems.nyoom;

public class DrivetrainCommand extends CommandBase {
  private nyoom m_Drivetrain;
  private DoubleSupplier m_leftSpeed;
  private DoubleSupplier m_rightSpeed;
  private DoubleSupplier m_strafeSpeed;


  /** Creates a new DrivetrainCommand. */
  public DrivetrainCommand(nyoom driveTrain, DoubleSupplier leftSpeed, DoubleSupplier rightSpeed, DoubleSupplier strafeSpeed) {
    m_Drivetrain = driveTrain;
    m_leftSpeed = leftSpeed;
    m_rightSpeed = rightSpeed;
    m_strafeSpeed = strafeSpeed;
    
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_Drivetrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double t_rightSpeed = m_rightSpeed.getAsDouble();
    double t_leftSpeed = m_leftSpeed.getAsDouble();
    double t_strafeSpeed = m_strafeSpeed.getAsDouble();
    m_Drivetrain.teleopDrive(t_leftSpeed, t_rightSpeed, t_strafeSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
