// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Feeder;
import frc.robot.subsystems.TurretAndShooter;
import frc.robot.subsystems.nyoom;

public class AutonomousCommand extends CommandBase {
  private Timer m_timer;
 private final TurretAndShooter m_TurretAndShooter;
 private final nyoom m_drivetrain;
 private final Feeder m_feeder;
  /** Creates a new AutonomousCommand. */
  public AutonomousCommand(TurretAndShooter turretAndShooter, nyoom drivetrain, Feeder feeder) {
    m_TurretAndShooter = turretAndShooter;
    m_drivetrain = drivetrain;
    m_feeder = feeder;
    m_timer = new Timer();

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_TurretAndShooter, m_drivetrain, m_feeder);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_timer.start();

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    
    if(m_timer.get() > 1.5){
      m_drivetrain.autonomousDrive();

    }
    if(m_timer.get()>3.5){
      m_drivetrain.autonomousDriveStop();
    }
    if (m_timer.get()>4) {
      m_TurretAndShooter.limelightTargeting();
    }
    if(m_timer.get()>6){
      m_TurretAndShooter.distanceShootCalculations();
    }
    if(m_timer.get() > 8){
      m_feeder.feederUpperIntakeManual();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
