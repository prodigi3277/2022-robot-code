// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    //network table stuffs
    public static int FIRST_INTOOK_CARGO_COLOR = 7;
    public static int SECOND_INTOOK_CARGO_COLOR = 8;
    public static int BALL_TWO_THERE_QUESTIONMARK = 9;

    //talon/falcon IDs
    public static int FRONT_RIGHT_DRIVE_ID = 8;
    public static int FRONT_LEFT_DRIVE_ID = 9;
    public static int BACK_LEFT_DRIVE_ID = 10;
    public static int BACK_RIGHT_DRIVE_ID = 11;
    public static int TURRET_TURN_ID = 5;
    public static int BOTTOM_SHOOTER_ID = 44;
    public static int TOP_SHOOTER = 35;
    public static int CLIMBER_ID = 26;

    public static int SPARK_PORT = 9;

    public static int CONTROLLER_PORT = 2;

    public enum XBOX_BUTTON {
        XBOX_A_BUTTON(1), XBOX_B_BUTTON(2), XBOX_X_BUTTON(3), XBOX_Y_BUTTON(4), XBOX_LEFT_SHOLDER_BUTTON(5),
        XBOX_RIGHT_SHOLDER_BUTTON(6), XBOX_BACK_BUTTON(7), XBOX_START_BUTTON(8), XBOX_LEFT_INDEX_TRIGGER(9),
        XBOX_RIGHT_INDEX_TRIGGER(10);

        private final int value;

        XBOX_BUTTON(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    } 


}
