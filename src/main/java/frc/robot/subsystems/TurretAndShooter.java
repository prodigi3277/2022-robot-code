// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.networktables.NetworkTableInstance;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class TurretAndShooter extends SubsystemBase {
  private final NetworkTableInstance nTableInstance = NetworkTableInstance.getDefault();
  private WPI_TalonFX shootMotorTop;
  private WPI_TalonFX shootMotorBottom;
  private WPI_TalonSRX turretTurnMotor;
  private double limelightHorizontalOffset;

  private boolean targetWtithLimelight = false;

//constants
  private static double MOUNT_DEGREES = 27;
  private static double ROBO_HEIGHT_INCHES = 26.5;
  private static double GOAL_HEIGHT_INCHES = 104;
  private static double LOWER_PORT_SHOOTER_SPEED = 0.25;

  /** Creates a new TurretAndShooter. */
  public TurretAndShooter() {
    try {
      shootMotorTop = new WPI_TalonFX(Constants.BOTTOM_SHOOTER_ID);
    } catch (Exception e) {
    System.out.println("top shooting motor not working");
    }
    try {
      shootMotorBottom = new WPI_TalonFX(Constants.TOP_SHOOTER);
    } catch (Exception e) {
    System.out.println("bottom shooting motor not working");
    }
    try {
      turretTurnMotor = new WPI_TalonSRX(Constants.TURRET_TURN_ID);
    } catch (Exception e) {
      System.out.println("turret turning motor not working");
    }
  }

  public double limelightDistanceEstimation(){
    double verticalOffset = nTableInstance.getTable("limelight").getEntry("ty").getDouble(0);
    double angleToGoal = MOUNT_DEGREES + verticalOffset;
    double radAngleToGoal = angleToGoal * (Math.PI / 180);
    double distanceToGoalViaLimelight = (GOAL_HEIGHT_INCHES - ROBO_HEIGHT_INCHES)
                                              /Math.tan(radAngleToGoal);
    return (distanceToGoalViaLimelight/12);
  }

  public void distanceShootCalculations(){
    if(targetWtithLimelight){
    int d_distance = ((int)limelightDistanceEstimation() + 3);
    switch (d_distance) {
      case 7:
      shootMotorTop.set(-0.165);
      shootMotorBottom.set(0.415);
        break;
      case 8:
      shootMotorTop.set(-0.185);
      shootMotorBottom.set(0.435);
      break;
      case 9:
      shootMotorTop.set(-0.185);
      shootMotorBottom.set(0.435);
      break;
      case 10:
      shootMotorTop.set(-0.195);
      shootMotorBottom.set(0.445);
      break;
      case 11:
      shootMotorTop.set(-0.205);
      shootMotorBottom.set(0.455);
      break;
      case 12:
      shootMotorTop.set(-0.21);
      shootMotorBottom.set(0.46);
      break;
      case 13:
      shootMotorTop.set(-0.215);
      shootMotorBottom.set(0.465);
      break;
      case 14:
      shootMotorTop.set(-0.23);
      shootMotorBottom.set(0.48);
      break;
      case 15:
      shootMotorTop.set(-0.245);
      shootMotorBottom.set(0.495);
      break;
      case 16:
      shootMotorTop.set(-0.26);
      shootMotorBottom.set(0.51);
      break;
    
      default:
//shootMotorTop.set(-0.15);
//shootMotorBottom.set(-0.4);
        break;
    }
  }
  else{
    shootMotorTop.set(LOWER_PORT_SHOOTER_SPEED+0.2);
    shootMotorBottom.set(LOWER_PORT_SHOOTER_SPEED + 0.33);
     }
  }

  //doesnt work, offseason project maybe?
  public void limelightShootNotFixedDistances(){
    double distanceHere = limelightDistanceEstimation();
   double topSpeed = (Math.pow(3, 0.0186*distanceHere)-Math.pow(2 ,0.5927*distanceHere) - (6.9447*distanceHere) -9.6897)/1000;
   System.out.println(topSpeed);
   double bottomSpeed = topSpeed + 0.25;
    shootMotorTop.set(topSpeed);
    shootMotorBottom.set(bottomSpeed);
  }

  public void limelightTargeting(){
    
    double distance = limelightDistanceEstimation();
    SmartDashboard.putNumber("distance inches", distance);
    limelightHorizontalOffset = nTableInstance.getTable("limelight").getEntry("tx").getDouble(0);
    double llSpeed = limelightHorizontalOffset * 0.03; 
    turretTurnMotor.set(-llSpeed);
    SmartDashboard.putNumber("horizoffval", limelightHorizontalOffset);
    SmartDashboard.putNumber("llSpeed", llSpeed);
    
  }

  
//end condition
  public void limelightStopTargeting(){
    turretTurnMotor.set(0); 
  }

//override
  public void turrenTurnLeft(){
    turretTurnMotor.set(-0.24);
    System.out.print(turretTurnMotor.getSelectedSensorPosition());
  }

  //override
  public void turretTurnRight(){
    turretTurnMotor.set(0.24);
    System.out.print(turretTurnMotor.getSelectedSensorPosition());
  }

  public void turretTurnStop(){
    turretTurnMotor.set(0);
  }

  //lower port overrride
  public void shootSlower(){
    shootMotorTop.set(LOWER_PORT_SHOOTER_SPEED);
    shootMotorBottom.set(LOWER_PORT_SHOOTER_SPEED);
  }

  public void shootAutonomous(){
    shootMotorTop.set(-0.55);
    shootMotorBottom.set(-0.55);
  }

  public void stopShoot(){
    shootMotorTop.set(0);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
