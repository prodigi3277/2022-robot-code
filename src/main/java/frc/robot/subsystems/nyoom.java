// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class nyoom extends SubsystemBase {
  /** Creates a new nyoom. */
  private WPI_TalonFX frontDriveRight;
  private WPI_TalonFX frontDriveLeft;
  private WPI_TalonFX backDriveLeft;
  private WPI_TalonFX backDriveRight;
  private DifferentialDrive theDifferentialDrive;
  private MotorControllerGroup driveLeft;
  private MotorControllerGroup driveRight;

  public nyoom() {
  try {
    frontDriveRight = new WPI_TalonFX(Constants.FRONT_RIGHT_DRIVE_ID); 
  } catch (Exception e) {
    System.out.println("front right drive motor not working");
  }
    
  try {
    frontDriveLeft = new WPI_TalonFX(Constants.FRONT_LEFT_DRIVE_ID); 
  } catch (Exception e) {
    System.out.println("front left drive motor not working");
  }
   
    try {
      backDriveLeft = new WPI_TalonFX(Constants.BACK_LEFT_DRIVE_ID);   
    } catch (Exception e) {
      System.out.println("back left drive motor not working");
    }
   
    try {
      backDriveRight = new WPI_TalonFX(Constants.BACK_RIGHT_DRIVE_ID); 
    } catch (Exception e) {
      System.out.println("back right drive motor not working");
    }

    driveRight = new MotorControllerGroup(frontDriveRight, backDriveRight);
    driveLeft = new MotorControllerGroup(frontDriveLeft, backDriveLeft); 
    theDifferentialDrive = new DifferentialDrive(driveLeft, driveRight);
    frontDriveLeft.setInverted(true);
    backDriveLeft.setInverted(true);

  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void teleopDrive(double leftSpeed, double rightSpeed, double strafeSpeed){
    theDifferentialDrive.arcadeDrive(leftSpeed, -rightSpeed);
    if(strafeSpeed>= 0.05 || strafeSpeed <= -0.05){
      frontDriveLeft.set(-strafeSpeed);
      backDriveLeft.set(strafeSpeed);
      frontDriveRight.set(strafeSpeed);
      backDriveRight.set(-strafeSpeed);
    }
   
  }

  public void autonomousDrive(){
      frontDriveLeft.set(0.2);
      backDriveLeft.set(0.2);
      frontDriveRight.set(0.2);
      backDriveRight.set(0.2);
  }
  public void autonomousDriveStop(){
    frontDriveLeft.set(0);
    backDriveLeft.set(0);
    frontDriveRight.set(0);
    backDriveRight.set(0);
}
}