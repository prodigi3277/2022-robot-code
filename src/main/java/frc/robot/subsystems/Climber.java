// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Timer;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;



public class Climber extends SubsystemBase {
  private Spark spark;
  private WPI_TalonFX climbMotor;
  private Timer m_timer;
  private Timer m_timerUp;

  /** Creates a new Shooter. */
  public Climber() {
    try {
    spark = new Spark(Constants.SPARK_PORT);
      
    } catch (Exception e) {
      System.out.println("spark isn't here");
    }
    try {
    climbMotor = new WPI_TalonFX(Constants.CLIMBER_ID);
      
    } catch (Exception e) {
      System.out.println("climber motor not working");
    }
    climbMotor.setNeutralMode(NeutralMode.Brake);
    m_timer = new Timer();
    m_timerUp = new Timer();
  }

  public void goUp(){
    m_timerUp.start();
  //  spark.set(1);
  if(m_timerUp.get() >= 0.5){
      climbMotor.set(-1);
    }
    if(m_timerUp.get() >= 2){
      climbMotor.set(0);
          }
    
    

  }

  public void goUpOverride(){
    m_timer.start();
    spark.set(1);
    if(m_timer.get() >= 0.5){
      climbMotor.set(-1);
    }
   
  }

  public void stopDoingThings(){
   
      climbMotor.set(0);
  }

  public void theBrake(){
    spark.set(-1);
  }

  public void theNoBrake(){
    spark.set(1);
  }

  public void goDown(){
    m_timer.start();
    spark.set(-1);
    if(m_timer.get() >= 1){
      climbMotor.set(1);
    }
    
   
    
  
  }



  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
