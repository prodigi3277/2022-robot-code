// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.ColorMatch;
import com.revrobotics.ColorSensorV3;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


public class Feeder extends SubsystemBase {
  private I2C.Port i2cPort;
  private ColorSensorV3 m_colorSensor;
  private final ColorMatch m_colorMatcher = new ColorMatch();
  private WPI_TalonSRX topBallIntake;
  private WPI_TalonSRX bottomBallIntakeFirst;
  private WPI_TalonSRX bottomBallIntake;
  private double LOWER_INTAKE_SPEED = -0.75;
 
  /** Creates a new Feeder. */
  public Feeder() {
    m_colorMatcher.addColorMatch(Color.kFirstBlue);
    m_colorMatcher.addColorMatch(Color.kFirstRed);
    try {
      topBallIntake = new WPI_TalonSRX(19);
    } catch (Exception e) {
      System.out.println("frontBallIntake not working");
    }
    try {
      bottomBallIntake = new WPI_TalonSRX(18);
    } catch (Exception e) {
      System.out.println("backBallIntake not working");

    }

    try {
      bottomBallIntakeFirst = new WPI_TalonSRX(17);
    } catch (Exception e) {
      System.out.println("backBallIntakeA not working");
    }
    try {
      i2cPort = I2C.Port.kOnboard;
    } catch (Exception e) {
      System.out.println("12cport not foudn");
    }
    try {
     m_colorSensor = new ColorSensorV3(i2cPort);
    } catch (Exception e) {
      System.out.println("colorsensor not found");
    }
   
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void feederLowerIntakeManual(){
     bottomBallIntakeFirst.set(LOWER_INTAKE_SPEED);
      bottomBallIntake.set(LOWER_INTAKE_SPEED);
  }
  public void feederLowerIntakeOverride(){
    bottomBallIntakeFirst.set(LOWER_INTAKE_SPEED);
      bottomBallIntake.set(LOWER_INTAKE_SPEED);
     }
     public void justTheFirstIntakeManual(){
       bottomBallIntakeFirst.set(LOWER_INTAKE_SPEED);
     }

     public void feederLowerIntakeReverseManual(){
      bottomBallIntake.set(-LOWER_INTAKE_SPEED);
     }

  public void feederUpperIntakeManual(){
    topBallIntake.set(-1);
  }

  public void stopFeedingMan(){
    topBallIntake.set(0);
    bottomBallIntake.set(0);
    bottomBallIntakeFirst.set(0);
  }

 

  public void proximityTestDebug(){
    
   // currentBlueValue = m_colorSensor.getBlue();
  //  currentRedValue = m_colorSensor.getRed();
  //  tColorMatchResultInt3 = m_colorSensor.getGreen();

  //  SmartDashboard.putNumber("current color blue",currentBlueValue);
  //  SmartDashboard.putNumber("current color red",currentRedValue);
 //   SmartDashboard.putNumber("current color green",tColorMatchResultInt3);
//S//martDashboard.putBoolean("sensor value", feederSensorA.get());

    SmartDashboard.putNumber("currentProximity", m_colorSensor.getProximity());

  }
}
